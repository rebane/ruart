#include "ruart_emu.h"
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <poll.h>

int16_t ruart_emu_read(int fd, void *buf, int16_t count)
{
	struct pollfd fds[1];
	int ret;

	if (count < 1)
		return count;

	fds[0].fd = fd;
	fds[0].events = POLLIN;

	ret = poll(fds, 1, 0);
	if (ret < 0)
		return -1;
	if (!ret)
		return 0;
	if (!(fds[0].revents & POLLIN))
		return 0;

	return read(fd, buf, count);
}

int16_t ruart_emu_write(int fd, const void *buf, int16_t count)
{
	if (count < 0)
		return count;

	return write(fd, buf, count);
}

