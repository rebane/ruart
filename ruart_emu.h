#ifndef _RUART_EMU_H_
#define _RUART_EMU_H_

#include <stdint.h>

int16_t ruart_emu_read(int fd, void *buf, int16_t count);
int16_t ruart_emu_write(int fd, const void *buf, int16_t count);

#endif

