#include "ruart.h"
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <termios.h>
#include <sys/ioctl.h>

#ifndef CMSPAR
#define CMSPAR        010000000000
#endif

int ruart_setup_standard(int fd, unsigned long long int baudrate, int bits,
						int parity, int stop, int flow)
{
	struct termios tio;

	memset(&tio, 0, sizeof(tio));

	tio.c_cflag = CLOCAL | CREAD | baudrate;

	if (bits == 5) {
		tio.c_cflag |= CS5;
	} else if(bits == 6) {
		tio.c_cflag |= CS6;
	} else if(bits == 7) {
		tio.c_cflag |= CS7;
	} else if(bits == 8) {
		tio.c_cflag |= CS8;
	} else {
		return -1;
	}

	if (parity == RUART_PARITY_EVEN) {
		tio.c_cflag |= PARENB;
	} else if (parity == RUART_PARITY_ODD) {
		tio.c_cflag |= PARENB | PARODD;
	} else if (parity == RUART_PARITY_MARK) {
		tio.c_cflag |= PARENB | CMSPAR | PARODD;
	} else if (parity == RUART_PARITY_SPACE) {
		tio.c_cflag |= PARENB | CMSPAR;
	}

	if (stop == 2)
		tio.c_cflag |= CSTOPB;

	if (flow == RUART_FLOW_RTSCTS)
		tio.c_cflag |= CRTSCTS;

	tio.c_iflag = IGNPAR | IGNBRK;
	tio.c_oflag = 0;
	tio.c_lflag = 0;
	tio.c_cc[VTIME] = 0;
	tio.c_cc[VMIN] = 1;

	return ioctl(fd, TCSETS, &tio);
}

