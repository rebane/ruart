#include "ruart.h"
#include <termios.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <linux/serial.h>

int ruart_open(char *device)
{
	return open(device, O_RDWR | O_NOCTTY | O_NONBLOCK);
}

int ruart_close(int fd)
{
	return close(fd);
}

char *ruart_parse_hints(char *hints, unsigned long long int *baudrate, int *bits, int *parity, int *stop, int *flow)
{
	char *device;
	char *dup, *s;
	int i, j;

	dup = strdup(hints);
	if (dup == NULL)
		return NULL;

	device = NULL;
	s = dup;

	for (i = 0; ; i++) {
		if ((dup[i] == ':') || (dup[i] == 0)) {
			dup[i] = 0;
			for (j = 0; s[j]; j++) {
				if (!isdigit(s[j]))
					break;
			}
			if (!s[j]) {
				*baudrate = strtoll(s, NULL, 0);
			} else if ((strlen(s) == 3) &&
					((s[0] == '5') || (s[0] == '6') || (s[0] == '7') || (s[0] == '8') || (s[0] == '9')) &&
					((toupper(s[1]) == 'N') || (toupper(s[1]) == 'E') || (toupper(s[1]) == 'O') || (toupper(s[1]) == 'M') || (toupper(s[1]) == 'S')) &&
					((s[2] == '1') || (s[2] == '2'))) {
				*bits = s[0] - '0';
				if (toupper(s[1]) == 'N')
					*parity = RUART_PARITY_NONE;
				else if (toupper(s[1]) == 'E')
					*parity = RUART_PARITY_EVEN;
				else if (toupper(s[1]) == 'O')
					*parity = RUART_PARITY_ODD;
				else if (toupper(s[1]) == 'M')
					*parity = RUART_PARITY_MARK;
				else if (toupper(s[1]) == 'S')
					*parity = RUART_PARITY_SPACE;

				*stop = s[2] - '0';
			} else {
				if (device != NULL)
					free(device);

				device = strdup(s);
			}

			if (hints[i] == 0)
				break;

			s = &dup[i + 1];
		}
	}

	free(dup);

	return device;
}

int ruart_open_hints(char *hints)
{
	char *device;
	unsigned long long int baudrate;
	int bits, parity, stop, flow;
	int i, fd;

	baudrate = 115200;
	bits = 8;
	parity = RUART_PARITY_NONE;
	stop = 1;
	flow = RUART_FLOW_NONE;

	device = ruart_parse_hints(hints, &baudrate, &bits, &parity, &stop, &flow);
	if (device == NULL)
		return -1;

	fd = ruart_open(device);
	free(device);
	if (fd < 0)
		return fd;

	i = ruart_setup(fd, baudrate, bits, parity, stop, flow);
	if (i < 0) {
		ruart_close(fd);
		return i;
	}

	return fd;
}

int ruart_setup(int fd, unsigned long long int baudrate, int bits, int parity,
							int stop, int flow)
{
	if (baudrate == 50) {
		return ruart_setup_standard(fd, B50, bits, parity, stop, flow);
	} else if (baudrate == 75) {
		return ruart_setup_standard(fd, B75, bits, parity, stop, flow);
	} else if (baudrate == 110) {
		return ruart_setup_standard(fd, B110, bits, parity, stop, flow);
	} else if (baudrate == 134) {
		return ruart_setup_standard(fd, B134, bits, parity, stop, flow);
	} else if (baudrate == 150) {
		return ruart_setup_standard(fd, B150, bits, parity, stop, flow);
	} else if (baudrate == 200) {
		return ruart_setup_standard(fd, B200, bits, parity, stop, flow);
	} else if (baudrate == 300) {
		return ruart_setup_standard(fd, B300, bits, parity, stop, flow);
	} else if (baudrate == 600) {
		return ruart_setup_standard(fd, B600, bits, parity, stop, flow);
	} else if (baudrate == 1200) {
		return ruart_setup_standard(fd, B1200, bits, parity, stop, flow);
	} else if (baudrate == 1800) {
		return ruart_setup_standard(fd, B1800, bits, parity, stop, flow);
	} else if (baudrate == 2400) {
		return ruart_setup_standard(fd, B2400, bits, parity, stop, flow);
	} else if (baudrate == 4800) {
		return ruart_setup_standard(fd, B4800, bits, parity, stop, flow);
	} else if (baudrate == 9600) {
		return ruart_setup_standard(fd, B9600, bits, parity, stop, flow);
	} else if (baudrate == 19200) {
		return ruart_setup_standard(fd, B19200, bits, parity, stop, flow);
	} else if (baudrate == 38400) {
		return ruart_setup_standard(fd, B38400, bits, parity, stop, flow);
	} else if (baudrate == 57600) {
		return ruart_setup_standard(fd, B57600, bits, parity, stop, flow);
	} else if (baudrate == 115200) {
		return ruart_setup_standard(fd, B115200, bits, parity, stop, flow);
	} else if (baudrate == 230400) {
		return ruart_setup_standard(fd, B230400, bits, parity, stop, flow);
	} else if (baudrate == 460800) {
		return ruart_setup_standard(fd, B460800, bits, parity, stop, flow);
	} else if (baudrate == 500000) {
		return ruart_setup_standard(fd, B500000, bits, parity, stop, flow);
	} else if (baudrate == 576000) {
		return ruart_setup_standard(fd, B576000, bits, parity, stop, flow);
	} else if (baudrate == 921600) {
		return ruart_setup_standard(fd, B921600, bits, parity, stop, flow);
	} else if (baudrate == 1000000) {
		return ruart_setup_standard(fd, B1000000, bits, parity, stop, flow);
	} else if (baudrate == 1152000) {
		return ruart_setup_standard(fd, B1152000, bits, parity, stop, flow);
	} else if (baudrate == 1500000) {
		return ruart_setup_standard(fd, B1500000, bits, parity, stop, flow);
	} else if (baudrate == 2000000) {
		return ruart_setup_standard(fd, B2000000, bits, parity, stop, flow);
	} else if (baudrate == 2500000) {
		return ruart_setup_standard(fd, B2500000, bits, parity, stop, flow);
	} else if (baudrate == 3000000) {
		return ruart_setup_standard(fd, B3000000, bits, parity, stop, flow);
	} else if (baudrate == 3500000) {
		return ruart_setup_standard(fd, B3500000, bits, parity, stop, flow);
	} else if (baudrate == 4000000) {
		return ruart_setup_standard(fd, B4000000, bits, parity, stop, flow);
	}
	return ruart_setup_extended(fd, baudrate, bits, parity, stop, flow);
}

int ruart_setup_rs485(int fd, int flags, unsigned int delay_rts_before_send,
					unsigned int delay_rts_after_send)
{
	struct serial_rs485 s485;

	memset(&s485, 0, sizeof(s485));

	if (flags & RUART_RS485_ENABLED)
		s485.flags |= SER_RS485_ENABLED;
	if (flags & RUART_RS485_RTS_ON_SEND)
		s485.flags |= SER_RS485_RTS_ON_SEND;
	if (flags & RUART_RS485_RTS_AFTER_SEND)
		s485.flags |= SER_RS485_RTS_AFTER_SEND;
	if (flags & RUART_RS485_RX_DURING_TX)
		s485.flags |= SER_RS485_RX_DURING_TX;
#ifdef SER_RS485_TERMINATE_BUS
	if (flags & RUART_RS485_TERMINATE_BUS)
		s485.flags |= SER_RS485_TERMINATE_BUS;
#endif
	s485.delay_rts_before_send = delay_rts_before_send;
	s485.delay_rts_after_send = delay_rts_after_send;
	return ioctl(fd, TIOCSRS485, &s485);
}

int ruart_flush(int fd)
{
	return tcflush(fd, TCIOFLUSH);
}

int ruart_pins(int fd, int ttl_high, int ttl_low)
{
	int ret, tio;

	ret = ioctl(fd, TIOCMGET, &tio);
	if (ret < 0)
		return ret;

	if (ttl_high & RUART_PIN_RTS)
		tio &= ~TIOCM_RTS;

	if (ttl_high & RUART_PIN_DTR)
		tio &= ~TIOCM_DTR;

	if (ttl_low & RUART_PIN_RTS)
		tio |= TIOCM_RTS;

	if (ttl_low & RUART_PIN_DTR)
		tio |= TIOCM_DTR;

	return ioctl(fd, TIOCMSET, &tio);
}

int ruart_major(int fd)
{
	struct stat s;
	int ret;

	ret = fstat(fd, &s);
	if (ret < 0)
		return ret;

	return major(s.st_rdev);
}

int ruart_minor(int fd)
{
	struct stat s;
	int ret;

	ret = fstat(fd, &s);
	if (ret < 0)
		return ret;

	return minor(s.st_rdev);
}

