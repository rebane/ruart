#ifndef _RUART_H_
#define _RUART_H_

#define RUART_PARITY_NONE          0
#define RUART_PARITY_EVEN          1
#define RUART_PARITY_ODD           2
#define RUART_PARITY_MARK          3
#define RUART_PARITY_SPACE         4

#define RUART_FLOW_NONE            0
#define RUART_FLOW_RTSCTS          1

#define RUART_PIN_RTS              0x01
#define RUART_PIN_DTR              0x02

#define RUART_RS485_ENABLED        0x01
#define RUART_RS485_RTS_ON_SEND    0x02
#define RUART_RS485_RTS_AFTER_SEND 0x04
#define RUART_RS485_RX_DURING_TX   0x08
#define RUART_RS485_TERMINATE_BUS  0x10

int ruart_open(char *device);
int ruart_close(int fd);
char *ruart_parse_hints(char *hints, unsigned long long int *baudrate, int *bits, int *parity, int *stop, int *flow);
int ruart_open_hints(char *hints);
int ruart_setup(int fd, unsigned long long int baudrate, int bits, int parity,
					int stop, int flow);
int ruart_setup_standard(int fd, unsigned long long int baudrate, int bits,
					int parity, int stop, int flow);
int ruart_setup_extended(int fd, unsigned long long int baudrate, int bits,
					int parity, int stop, int flow);
int ruart_setup_rs485(int fd, int flags, unsigned int delay_rts_before_send,
					unsigned int delay_rts_after_send);
int ruart_flush(int fd);
int ruart_pins(int fd, int ttl_high, int ttl_low);
int ruart_major(int fd);
int ruart_minor(int fd);

#endif

